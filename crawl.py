# coding=utf-8
import datetime
import os
import time
import codecs

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
# driver = webdriver.Firefox()

chromeOptions = webdriver.ChromeOptions()
prefs = {'profile.managed_default_content_settings.images':2}
chromeOptions.add_experimental_option("prefs", prefs)
chromeOptions.add_argument('headless')
chromeOptions.add_argument('window-size=1200x600')
# chromedriver = "/Users/oraviv/git/pythons/chromedriver"
chromedriver = "/home/ohad/git/net_hamishpat/chromedriver"
os.environ["webdriver.chrome.driver"] = chromedriver
driver = webdriver.Chrome(chromedriver, chrome_options=chromeOptions)


driver.get("https://www.court.gov.il/NGCS.Web.Site/SearchCase/CasesSearchView.aspx")
driver.find_element_by_id("Header1_UpperMenu1_tdSearchCase").click()
driver.find_element_by_id("Header1_UpperMenu1_tdCaseDate").click()

driver.find_element_by_xpath("//select[@id='courtIDDropDown']/option[text()='שלום תל אביב - יפו']").click()
driver.find_element_by_id("buttonsGroup_searchButton").click()

time.sleep(1)

output_file = 'net_hamishpat.txt'
last_date_in_file = max([l for l in open(output_file) if l[0:3]=='###']).split('|')[1]
# date = datetime.datetime(2012,1,1)
cur_date = datetime.datetime.strptime(last_date_in_file, '%Y-%m-%d')
last_date = cur_date + datetime.timedelta(days=50)

while cur_date < last_date:
    cur_date += datetime.timedelta(days=1)

    elem = driver.find_element_by_id("openDateCalendar")
    dt = cur_date.strftime('%Y-%m-%d')
    driver.execute_script("arguments[0].setAttribute('value','"+dt+"')", elem)
    driver.find_element_by_id("buttonsGroup_searchButton").click()

    try:
        time.sleep(3)
        more = True
        page_num = 1
        sub_sum_cases = 0
        while more:

            f = codecs.open(output_file, "a", "utf-8")

            if page_num==1:
                num_in_dt_elems = driver.find_elements_by_xpath("//span[@style='font-weight:bold;']")
                if len(num_in_dt_elems) == 2:
                    num_in_dt = num_in_dt_elems[1].text
                    f.write("###|" + dt + "|" + str(num_in_dt) + "\n")
                    print time.strftime('[%x %X]'),"in date:",dt,"need to find",num_in_dt,"cases"

            cases = driver.find_elements_by_xpath("//tr[@t='SearchResultsDG_SearchResultsDGrt']")
            sub_sum_cases+=len(cases)
            print "found:",sub_sum_cases,"cases"

            for blah in cases:
                line = [e.text for e in blah.find_elements_by_xpath(".//span")]
                f.write(dt+"|"+str(page_num)+"|"+"|".join(line)+"\n")
            f.close()

            nexts = driver.find_elements_by_xpath("//a[@title='הצג דף הבא']")
            if len(nexts) == 1:
                nexts[0].click()
                page_num += 1
                time.sleep(0.2)
            else:
                more = False

    except Exception as e:
        print e
        driver.close()

driver.close()
